# Tips de seguridad informatica
### Dirección General de Tecnologías de la Información y la Comunicación
### Ministerio de Relaciones Exteriores y Culto

1. NUNCA compartas tu contraseña con otra persona.
2. No conecte a su PC dispositivos de almacenamiento externo cuyo origen desconozca.[^1]
3. No dejar dispositivos de almacenamiento externo conectados a la PC mientras esté fuera de la oficina. 
4. Genere contraseñas alfanuméricas y nunca las almacene de tal forma que otros usuarios puedan acceder a ellas facilmente.
5. Mantenga un escritorio limpio. No deje sus contraseñas o información personal a la vista.
6. 



[^1: Los dispositivos de almacenamiento externo pueden incluir: pendrives o discos duros]